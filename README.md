# HTTPS at Home

This project shows how to serve a web application over HTTPS on a home network.

Technologies used include:

- Ubuntu 16.04
- Docker Swarm
- Traefik (with Let's Encrypt)
- Freenom (dot .tk domains)
- NGINX

Here are the steps for recreating this in your own network:

1) Configure router
2) Obtain free .tk domain (no credit card required)
3) Deploy docker swarm stack
4) Check connection and diagnose issues

## Configure router

Access your router's GUI. For me this means visiting `routerlogin.net` and typing the username and password for my router (should be printed on the router). In the advanced settings, enable port forwarding and set up a rule that forwards port 443 to port 443 of the machine that will be serving as the swarm's manager.

## Obtain free .tk domain

First, register an account with freenom.com and get a .tk domain. Then go to `Services > My Domains` and click on `Manage Domain` for the domain you have registered, then click on the tab that says `Manage Freenom DNS`. Here we will add a record. Leave the `Name` section blank. Type should be the default, `A`, TTL should be `3600`, and the target should be your public IP. You can find this by googling "what is my ip". Save the changes and verify that the record shows up at the top of the page.

## Deploy the stack to Docker Swarm

Setting up a swarm cluster is simple. Select one computer that will be the `manager` and one or more computers that will be `workers`. A recent version of docker should be running on all computers. On the manager, run `docker swarm init`, and then copy the `join` command to the other computers you want include in your cluster as workers.

Next, make sure you have this repo cloned to the swarm manager. Be sure to replace the following values:

- `stack.yml`: under `services > nginx > labels`, replace `mycustomdomain.tk` with your domain (it doesn't have to be a .tk domain)

- `traefik.toml`: under `[docker]`, replace the value for `domain` with your domain and under `[email]` replace the email address with your email address.

Finally, run the following command from the same directory where the file `stack.yml` lives:

```
docker stack deploy --compose-file stack.yml stack
```

You can replace the final `stack` word with whatever you want to call this stack.

## Check connection and diagnose issues

Now visit `https://your-domain-name.tk` and you should see the response from `prod.conf`: "It works."

I'm currently having an issue that is causing requests to the domain to take a long time to respond, sometimes around one minute.